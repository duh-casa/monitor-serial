#!/bin/bash
echo "Downloading package"
if [ ! -f xresprobe_0.4.24ubuntu9_amd64.deb ]; then
  wget "http://archive.ubuntu.com/ubuntu/pool/universe/x/xresprobe/xresprobe_0.4.24ubuntu9_amd64.deb"
fi

echo "Installing package"
if [ $(dpkg-query -W -f='${Status}' xresprobe 2>/dev/null | grep -c "ok installed") -eq 0 ];
then
  sudo dpkg -i xresprobe_0.4.24ubuntu9_amd64.deb;
fi

echo "Configuring OS"
sudo mount -o remount,exec /dev


